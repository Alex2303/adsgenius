import React from 'react';
import './App.css';
import Header from './components/Header';
import Generateur from './components/Generateur';

export default class App extends React.Component {
  render() {
    return (
      <div className="App" 
        style={{backgroundImage: `url(wp-content/reactpress/apps/adsgenius-generator/public/assets/background-form.png)`, 
        backgroundSize: 'cover', 
        width: '100%'}}
      >
        <Header />
        <Generateur />
      </div>
    );
  }
}



