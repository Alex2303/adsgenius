import React from "react";
import Nav from "./Nav";
import { Link, scroller } from 'react-scroll';
import ButtonPaillettes from "./ButtonPaillettes";

export default class Header extends React.Component {

    scrollTo() {
        scroller.scrollTo('scroll-to-element', {
            duration: 800,
            delay: 0,
            smooth: 'easeInOutQuart'
        })
    }


    render() {
        return (
            <header>
                <Nav />
                <div className="presentation"
                    style={{ backgroundImage: `url(wp-content/reactpress/apps/adsgenius-generator/public/assets/background-header.png)`, backgroundSize: 'cover' }}
                >


                    <div className="presentation_left">
                        <h3 className="white bold mb-20">Obtenez des messages marketing efficaces en quelques clics grâce à notre outil de génération de contenu</h3>
                        <Link to="generateur" smooth={true} duration={500}>
                            <ButtonPaillettes />
                        </Link>
                    </div>
                    <div className="presentation_right"
                        style={{
                            backgroundImage: `url(wp-content/reactpress/apps/adsgenius-generator/public/assets/header-img.png)`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center',
                            width: '100%',
                            height: '420px'
                        }}

                    >

                    </div>
                </div>
            </header>
        )
    }

}
