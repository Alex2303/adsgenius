import React from 'react';
import { Link, scroller } from 'react-scroll';

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isMobile: false,
            menuOpen: false
        }
        this.toggleMenu = this.toggleMenu.bind(this);
    }

    scrollTo() {
        scroller.scrollTo('scroll-to-element', {
            duration: 800,
            delay: 0,
            smooth: 'easeInOutQuart'
        })
    }

    toggleMenu() {
        this.setState(prevState => ({
            menuOpen: !prevState.menuOpen
        }));
    }

    render() {
        return (
            <div>
                <nav id="navbar">
                    <Link to="main" className='logo'
                        style={{backgroundImage: `url(wp-content/reactpress/apps/adsgenius-generator/public/assets/logo.svg)`, 
                        backgroundSize: 'contain', 
                        backgroundRepeat: 'no-repeat',
                        margin: '0.75rem 1rem',
                        height: '50px'}}
                        >
                            
                    </Link>
                </nav>
            </div>
        );
    }
}
