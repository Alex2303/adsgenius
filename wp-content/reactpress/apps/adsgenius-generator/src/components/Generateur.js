import React, { useState } from 'react';
import axios from 'axios';
import RadioBtn from './RadioBtn';
import Card from './Card';
import ButtonPaillettes from './ButtonPaillettes';
import AdSense from './Adsense';

function Generateur() {
    const [productOrService, setProductOrService] = useState('produit');
    const [subject, setSubject] = useState('');
    const [tone, setTone] = useState('professionnal');
    const [campaignObjective, setCampaignObjective] = useState('notorious');
    const [fieldOfActivity, setFieldOfActivity] = useState('');
    const [posts, setPosts] = useState([]);
    const [showAd, setShowAd] = useState(false);

    //on initialise loading sur false
    const [loading, setLoading] = useState(false);

    const handleSubmit = async (event) => {
        event.preventDefault();
        setShowAd(true);
        //on change la valeur de loading en true, o
        setLoading(true);
        //phrase demandée à chatGPT
        let prompt =
            `Imagine que tu es un expert en marketing digital.
            Rédige 3 propositions de publication comprenant un titre et un contenu, qui seront destinés à Facebook ads, en te basant du
            produit ou service : ${productOrService} ${subject}
            dans le domaine d'activité ${fieldOfActivity}
            avec un ton ${tone} et en visant à atteindre l'objectif ${campaignObjective} de Facebook ads`;

        const client = axios.create({
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${process.env.REACT_APP_OPENAI_API_KEY}`,
            },
        });

        const params = {
            prompt: prompt,
            model: "text-davinci-003",
            max_tokens: 2000,
            temperature: 0.5,
        };

        client
            .post("https://api.openai.com/v1/completions", params)
            .then((result) => {
                const postsData = result.data.choices[0].text.split("\n\n");
                setPosts(postsData);

                setLoading(false);
            })
            .catch((err) => {
                console.log(err);
            });


    }

    //Affichage
    return (
        <div id="generateur">
            <div className='header_generator'>
                <h5 className='white bold mb-20'>
                    Générez vos titres, vos descriptions, vos appels à l'action.
                </h5>
                <p className='white'>Remplissez le formulaire avec vos intentions de campagne, vous n'aurez plus qu'à publier</p>
            </div>
            <div className='box-generator'>
                <form
                    className='generator'
                    onSubmit={handleSubmit}
                >
                    {showAd && <AdSense />}
                    <div className='container'>
                        <h5>Quel est votre Domaine d'activité ?</h5>
                        <input
                            placeholder="Quel est votre domaine d'activité"
                            type="text"
                            value={fieldOfActivity}
                            onChange={(e) => { setFieldOfActivity(e.target.value) }}
                        />
                    </div>

                    <br />

                    <div className='container'>
                        <h5>Vous souhaitez parler d'un produit ou d'un service ?</h5>
                        <div
                            className='product-or-service'
                        >
                            <RadioBtn
                                name="produit"
                                id="produit"
                                value="produit"
                                text="Produit"
                                onChange={() => { setProductOrService("produit") }}
                                checked={productOrService === 'produit'}
                            />
                            <RadioBtn
                                name="service"
                                id="service"
                                value="service"
                                text="Service"
                                onChange={() => { setProductOrService("service") }}
                                checked={productOrService === 'service'}
                            />
                        </div>

                    </div>

                    <br />

                    <div className='container'>
                        <h5>Dite m'en un peu plus sur votre {productOrService} ?</h5>
                        <input
                            placeholder={productOrService === "produit" ? "crème de soin pour visage" : "location de voitures"}
                            type="text"
                            value={subject}
                            onChange={(e) => { setSubject(e.target.value) }}
                        />
                    </div>

                    <br />

                    <div className='container'>
                        <h5>Choisissez le ton de votre publication</h5>
                        <div className='tone'>
                            <RadioBtn
                                name="professionnal"
                                id="professionnal"
                                value="professionnal"
                                text="Professionnel"
                                onChange={() => { setTone("professionnal") }}
                                checked={tone === 'professionnal'}
                            />
                            <RadioBtn
                                name="enthusiastic"
                                id="enthusiastic"
                                value="enthusiastic"
                                text="enthousiaste"
                                onChange={() => { setTone("enthusiastic") }}
                                checked={tone === 'enthusiastic'}
                            />
                            <RadioBtn
                                name="humorous"
                                id="humorous"
                                value="humorous"
                                text="humoristique"
                                onChange={() => { setTone("humorous") }}
                                checked={tone === 'humorous'}
                            />
                            <RadioBtn
                                name="anxious"
                                id="anxious"
                                value="anxious"
                                text="anxiogène"
                                onChange={() => { setTone("anxious") }}
                                checked={tone === 'anxious'}
                            />
                        </div>

                    </div>

                    <br />

                    <div className='container'>
                        <h5>Quel est votre objectif de campagne ?</h5>
                        <div className='campaign-objective'>
                            <RadioBtn
                                name="notorious"
                                id="notorious"
                                value="notorious"
                                text="Notoriété"
                                description="Créez des publicités localisées et diffusez les auprès des personnes qui se trouvent à proximité afin d'encourager la fréquentation de vos boutiques."
                                onChange={() => { setCampaignObjective("notorious") }}
                                checked={campaignObjective === 'notorious'}
                            />
                            <RadioBtn
                                name="sales"
                                id="sales"
                                value="sales"
                                text="Ventes"
                                description="Rédirigez vers une destination, par exemple votre site web, applicaiton ou évènements Facebook."
                                onChange={() => { setCampaignObjective("sales") }}
                                checked={campaignObjective === 'sales'}
                            />
                            <RadioBtn
                                name="interactions"
                                id="interactions"
                                value="interactions"
                                text="Interactions"
                                description="Obtenez plus de message, de vue de vidéo d'interaction avec vos publications, de mentions j'aime sur votre page, et des réponses à vos évènements"
                                onChange={() => { setCampaignObjective("interactions") }}
                                checked={campaignObjective === 'interactions'}
                            />
                            <RadioBtn
                                name="appPromotion"
                                id="appPromotion"
                                value="appPromotion"
                                text="Promotion de l'application"
                                description="Trouver de nouvelles personnes qui installeront l'application et continueront à l'utiliser."
                                onChange={() => { setCampaignObjective("appPromotion") }}
                                checked={campaignObjective === 'appPromotion'}
                            />
                            <RadioBtn
                                name="trafic"
                                id="trafic"
                                value="trafic"
                                text="Trafic"
                                description="Votre objectif de campagne est l'objectif commercial que vous esperez atteindre en diffuant vos pubs"
                                onChange={() => { setCampaignObjective("trafic") }}
                                checked={campaignObjective === 'trafic'}
                            />
                            <RadioBtn
                                name="prospection"
                                id="prospection"
                                value="prospection"
                                text="Prospection"
                                description="Attire les prospects pour votre entreprise ou votre marque"
                                onChange={() => { setCampaignObjective("prospection") }}
                                checked={campaignObjective === 'prospection'}
                            />
                        </div>
                    </div>

                    <ButtonPaillettes />
                </form>
                <div className='result'>
                    {loading
                        ?
                        (<span className='gif'
                            style={{backgroundImage: `url(wp-content/reactpress/apps/adsgenius-generator/public/assets/load.gif)`, 
                            display: 'block',
                            margin: 'auto',
                            backgroundSize: 'contain', 
                            backgroundPosition: 'center',
                            width: '50px',
                            height: '50px'
                    }}
                        >

                        </span>)
                        :
                        (
                            <div>
                                {posts.map((post, index) => {


                                    const [title, content] = post.split("\n");
                                    if (!title || !content) return null;
                                    return (
                                        <Card key={index} title={title} content={content} />

                                    )
                                })}
                            </div>)
                    }
                </div>
            </div>
        </div>

    );
}

export default Generateur;


