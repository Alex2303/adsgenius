const RadioButton = ({ name, id, value, onChange, checked, text, description }) => {
    return (
        // <div className="test">
            <label htmlFor={id} className="radio-label">
                
                <input
                className="radio-input"
                type="radio"
                name={name}
                id={id}
                value={value}
                onChange={onChange}
                checked={checked}
                />
                <span className="custom-radio" />

                <div className="radio-btn-title">
                    {text}
                </div>

                <div className="radio-btn-description">
                    
                    {description}
                </div>
                
            </label>
    //   </div>
    )
  }

  export default RadioButton;