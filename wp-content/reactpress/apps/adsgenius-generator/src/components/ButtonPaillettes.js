function ButtonPaillettes() {
    return (
        <button className="btn">
                <div className="circle">
                    <div className="paillettes"
                        style={{backgroundImage: `url(wp-content/reactpress/apps/adsgenius-generator/public/assets/paillettes.png)`, 
                        backgroundSize: 'contain', 
                        width: '20px',
                        height: '20px'
                    }}
                    >
                    </div>
                </div>
                <span>Essayer !</span>
        </button>
    )
}

export default ButtonPaillettes;