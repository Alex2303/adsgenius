<?php
/*
Template Name: Adsgenius
*/
?>

<!DOCTYPE html>
<html>
  <head>
    <title><?php the_title(); ?></title>
  </head>
  <body>
    <?php the_content(); ?>
  </body>
  <script src="<?php echo get_template_directory_uri(); ?>/react-app.js"></script>
</html>

